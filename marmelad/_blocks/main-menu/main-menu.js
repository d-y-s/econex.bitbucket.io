function menuMore() {
    $('.main-menu__nav ul').oneLineMenu({
        minWidth: 1024
    });

    $('.dropdown-wrap').on('click', function () {
        $(this).find('ul').stop().fadeToggle();
    })

    $(document).on('click', function (e) {
        if ($(e.target).closest('.main-menu__nav').length) {
            return;
        }

        $('.dropdown-wrap ul').fadeOut();
    });
}

menuMore();