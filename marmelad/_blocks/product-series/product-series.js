function ProductSeriesImgPath() {
    
    var getProductSeriesImgPath = $('.js-product-series-img-path').find('img').attr('src'),
        styleInlineBgImg = 'background-image: url(' + getProductSeriesImgPath + ');';
        $('.js-product-series-img-path').attr('style', styleInlineBgImg);
}

ProductSeriesImgPath();

if (window.matchMedia("(max-width: 1023px)").matches) {
    $('.js-product-series-head').insertBefore('.product-series__section');
}
