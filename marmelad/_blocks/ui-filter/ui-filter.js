$.fn.digitsFilter = function () {

    $(this).on('keydown', function (e) {

        // Allow: backspace, delete, tab, escape, enter and //.
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 || //, 110, 190
            // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {

            return;
        }

        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
}

$('.ui-filter__range').each(function () {

    var $range = $(this);
    var $inputs = $range.closest('.ui-filter__range-controlls').find('.from, .to');
    var $inputMin = $inputs.eq(0);
    var $inputMax = $inputs.eq(1);

    function setToInputsRangeData($_this, upd) {

        var from = $_this.data("from"),
            to = $_this.data("to"),
            postfix = $_this.data('postfix');

        if (upd) {
            $inputMin.val(postfix ? from + postfix : from);
            $inputMax.val(postfix ? to + postfix : to);
        }

    }

    $inputs.digitsFilter();

    $range.ionRangeSlider({
        onStart: function () {
            setToInputsRangeData($range, true);
        }
    });

    //--------------------------------------

    var updateInputs = true;

    $range.on("change", function () {

        setToInputsRangeData($range, updateInputs);
        updateInputs = true;

    });

    $inputs.on('change', function () {

        //accounting.formatNumber(4999.99, 0, " ");

        updateInputs = false;

        var inputMinVal = Number($inputMin.val().replace(/\D+/g, ''));
        var inputMaxVal = Number($inputMax.val().replace(/\D+/g, ''));
        var postfix = $range.data('postfix') || 0;

        if ($(this)[0] == $inputMin[0]) {

            if (inputMinVal > inputMaxVal) {
                $inputMin.val(inputMaxVal + postfix);
            }

            if (inputMinVal < $range.data('min')) {
                $inputMin.val($range.data('min') + postfix);
            }

        } else {

            if (inputMaxVal < inputMinVal) {
                $inputMax.val(inputMinVal + postfix);
            }

            if (inputMaxVal > $range.data('max')) {
                $inputMax.val($range.data('max') + postfix);
            }

        }

        $range.data('ionRangeSlider').update({
            from: $inputMin.val().replace(/\D+/g, ''),
            to: $inputMax.val().replace(/\D+/g, '')
        });
    });

});

$('.js-ui-filter-title').on('click', function () {
    $('.js-ui-filter-form').slideToggle();
})

if (window.matchMedia("(min-width: 960px)").matches) {
    $(".ui-filter").stick_in_parent({
        offset_top: 111
    });
}
$('.js-btn-show-more').on('click', function (event) {
    event.preventDefault();
    $('.js-show-more').addClass('show-more');
    $(this).hide();
});

// filter docs
$('.js-item-doc').on('click', function () {
    $('.js-docs-content').hide();
    var $thisItem = $(this),
        $thisData = $thisItem.attr('data-filterDoc');
    if ($thisData === "certificates") {
        $thisItem.addClass('is-active-doc');
        $thisItem.siblings().removeClass('is-active-doc')
        $('[data-docContent=certificates]').show();
    }
    if ($thisData === "passports") {
        $thisItem.addClass('is-active-doc');
        $thisItem.siblings().removeClass('is-active-doc')
        $('[data-docContent=passports]').show();
    }
    if ($thisData === "ies") {
        $thisItem.addClass('is-active-doc');
        $thisItem.siblings().removeClass('is-active-doc')
        $('[data-docContent=ies]').show();
    }

});

// function ieVersion() {
//     var ua = window.navigator.userAgent;
//     if (ua.indexOf("Trident/7.0") > -1) {
//         $('html').hide();
//     }

//     console.log(ua, '+')
// } 

// ieVersion();

// var uiFilter = $('.ui-filter');
// $(window).scroll(function () {
//   if ($(this).scrollTop() > 350) {
//     uiFilter.addClass('fixed-filter-active');
//   } else if ($(this).scrollTop() <= 350) {
//     uiFilter.removeClass('fixed-filter-active');
//   }
// });
// if (window.matchMedia("(min-width: 1360px)").matches) {
//   $(function() {
//     var $window = $(window);
//     var $sidebar = $(".ui-filter");
//     var $sidebarTop = $sidebar.position().top;
//     var $sidebarHeight = $sidebar.height();
//     var $footer = $('.questions');
//     var $footerTop = $footer.position().top;

//     $window.scroll(function(event) {
//       $sidebar.addClass("fixed-filter-active");
//       var $scrollTop = $window.scrollTop() + 111;
//       var $topPosition = Math.max(0, $sidebarTop - $scrollTop);

//       if ($scrollTop + $sidebarHeight > $footerTop) {
//         var $topPosition = Math.min($topPosition, $footerTop - $scrollTop - $sidebarHeight);
//       }

//       $sidebar.css("top", $topPosition + 111);

//       if ($scrollTop <= 41) {
//         $sidebar.removeClass("fixed-filter-active");
//       }
//     });
//   });
// }
