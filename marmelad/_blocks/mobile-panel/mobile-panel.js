$('.js-btn-burger').on('click', function () {
    $('.js-menu-show').toggleClass('is-active-menu');
    $('.js-mobile-menu').toggleClass('is-active-mobile-menu');
});

$('.js-btn-close').on('click', function () {
    $('.js-menu-show').removeClass('is-active-menu');
    $('.js-mobile-menu').removeClass('is-active-mobile-menu');
})