function selectSityType2() {
    var selectedSity = $('.js-selected-sity-type2'),
        listSity = $('.js-list-sity-type2');
    selectedSity.on('click', function () {
        listSity.stop().slideToggle();
    });

    $(document).on('click', function (e) {
        if ($(e.target).closest('.select-sity-type2').length) {
            return;
        }

        listSity.slideUp();
    });
}

selectSityType2();
