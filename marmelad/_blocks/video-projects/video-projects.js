// Video Modal

var videoModal = $('[data-remodal-id=video]').remodal();

$('.js-preview').on('click', function (event) {
    event.preventDefault();
    var $this = $(this),
        videoSrc = $this.attr('href'),
        hiddenTitle = $this.find('.hidden-title-video').clone(),
        hiddenText = $this.find('.hidden-text-video').clone(),
        inRemodalVideo = $this.closest('.app').find('.popup-video');

    inRemodalVideo.find('iframe').attr('src', videoSrc);
    inRemodalVideo.find('.js-inside-title').append(hiddenTitle);
    inRemodalVideo.find('.js-inside-text').append(hiddenText);
    videoModal.open();
    console.log(hiddenTitle)
});
$(document).on('closed', '[data-remodal-id=video]', function (e) {
    $('[data-remodal-id="video"] iframe').attr('src', '');
    $('[data-remodal-id="video"] .js-inside-title').find('.hidden-title-video').remove();
    $('[data-remodal-id="video"] .js-inside-text').find('.hidden-text-video').remove();
});

// Video Modal END
