function selectSity() {
    var selectedSity = $('.js-selected-sity'),
        listSity = $('.js-list-sity');
    if (window.matchMedia("(min-width: 1025px)").matches) {
        selectedSity.on('click', function () {
            listSity.stop().fadeToggle();
        });

        $(document).on('click', function (e) {
            if ($(e.target).closest('.select-sity').length) {
                return;
            }

            listSity.fadeOut();
        });
    } else {
        selectedSity.on('click', function () {
            listSity.stop().slideToggle();
        });

        $(document).on('click', function (e) {
            if ($(e.target).closest('.select-sity').length) {
                return;
            }

            listSity.slideUp();
        });
    }
}

selectSity();