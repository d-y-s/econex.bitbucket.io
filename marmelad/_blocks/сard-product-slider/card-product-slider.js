var productSlider = $('.card-product-slider__top-slider'),
    productItems = productSlider.find('.card-product-slider__top-slider-item'),
    productThumbs = $('.card-product-slider__bottom-slider');

$.each(productItems, function (i, el) {
    var classActive = i == 0 ? 'is-active' : '';
    var item = $('<div class="card-product-slider__bottom-slider-item ' + classActive + '" data-index="' + i + '"><img src="' + $(el).data('thumb') + '" alt=""></div>');

    item.appendTo(productThumbs);
});

productSlider.addClass('owl-carousel').owlCarousel({
    mouseDrag: false,
    items: 1,
    animateOut: 'fadeOut',
    nav: true,
    dots: false,
    navText: ['<svg width="11" height="18" viewBox="0 0 11 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 17L9 9L0.999999 1" stroke="white" stroke-width="2"/></svg>', '<svg width="11" height="18" viewBox="0 0 11 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 17L9 9L0.999999 1" stroke="white" stroke-width="2"/></svg>'],
    onChanged: function onChanged(event) {
        var $thisIndex = event.item.index;
        $('.card-product-slider__bottom-slider-item').removeClass('is-active');
        $('.card-product-slider__bottom-slider-item[data-index="' + $thisIndex + '"]').addClass('is-active');
    },
    onInitialize: function onInitialize(event) {
        productThumbs.addClass('owl-carousel').owlCarousel({
            items: 4,
            margin: 4,
            nav: false,
            navText: ['<svg width="11" height="18" viewBox="0 0 11 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 17L9 9L0.999999 1" stroke="white" stroke-width="2"/></svg>', '<svg width="11" height="18" viewBox="0 0 11 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 17L9 9L0.999999 1" stroke="white" stroke-width="2"/></svg>'],
            mouseDrag: false
        });
    }
});

$('body').on('click', '.card-product-slider__bottom-slider-item', function () {
    productSlider.trigger('to.owl.carousel', [$(this).data('index'), 300]);
    $('.card-product-slider__bottom-slider-item').removeClass('is-active');
    $('.card-product-slider__bottom-slider-item[data-index="' + $(this).data('index') + '"]').addClass('is-active');
});