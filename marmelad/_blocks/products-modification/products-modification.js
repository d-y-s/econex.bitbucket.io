$('.products-modification__tabs-switches').on('click', '.js-tabs-swither:not(.is-active-tabs-swither)', function () {
    $(this)
        .addClass('is-active-tabs-swither')
        .siblings()
        .removeClass('is-active-tabs-swither');

    $('.products-modification__tabs-container')
        .find('.js-tabs-content')
        .removeClass('is-active-tabs-content')
        .hide().eq($(this).index()).fadeIn();
});

$('.js-btn-archive').on('click', function () {
    $(this).toggleClass('is-active-btn-archive');
    $('.js-archive-content').slideToggle();
})

if (window.matchMedia("(max-width: 479px)").matches) {
    $('.js-btn-archive').find('span').text('архивные модификации')
}