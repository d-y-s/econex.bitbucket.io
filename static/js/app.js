"use strict";

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

/* ^^^
 * Глобальные-вспомогательные функции
 * ========================================================================== */

/**
  * Возвращает HTML-код иконки из SVG-спрайта
  *
  * @param {String} name Название иконки из спрайта
  * @param {Object} opts Объект настроек для SVG-иконки
  *
  * @example SVG-иконка
  * getSVGSpriteIcon('some-icon', {
  *   tag: 'div',
  *   type: 'icons', // colored для подключения иконки из цветного спрайта
  *   class: '', // дополнительные классы для иконки
  *   mode: 'inline', // external для подключаемых спрайтов
  *   url: '', // путь до файла спрайта, необходим только для подключаемых спрайтов
  * });
  */
function getSVGSpriteIcon(name, opts) {
  opts = _extends({
    tag: 'div',
    type: 'icons',
    "class": '',
    mode: 'inline',
    url: ''
  }, opts);
  var external = '';
  var typeClass = '';

  if (opts.mode === 'external') {
    external = "".concat(opts.url, "/sprite.").concat(opts.type, ".svg");
  }

  if (opts.type !== 'icons') {
    typeClass = " svg-icon--".concat(opts.type);
  }

  opts["class"] = opts["class"] ? " ".concat(opts["class"]) : '';
  return "\n    <".concat(opts.tag, " class=\"svg-icon svg-icon--").concat(name).concat(typeClass).concat(opts["class"], "\" aria-hidden=\"true\" focusable=\"false\">\n      <svg class=\"svg-icon__link\">\n        <use xlink:href=\"").concat(external, "#").concat(name, "\"></use>\n      </svg>\n    </").concat(opts.tag, ">\n  ");
}
/* ^^^
 * JQUERY Actions
 * ========================================================================== */


$(function () {
  'use strict';
  /**
   * определение существования элемента на странице
   */

  $.exists = function (selector) {
    return $(selector).length > 0;
  };

  if (window.matchMedia("(max-width: 1279px)").matches) {
    $('.catalog-products__items').addClass('owl-carousel').owlCarousel({
      mouseDrag: false,
      items: 5,
      nav: true,
      margin: 0,
      loop: false,
      dots: false,
      navText: ['<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M12.7071 4.29289C12.3166 3.90237 11.6834 3.90237 11.2929 4.29289C10.9024 4.68342 10.9024 5.31658 11.2929 5.70711L16.5858 11H5C4.44772 11 4 11.4477 4 12C4 12.5523 4.44772 13 5 13H16.5858L11.2929 18.2929C10.9024 18.6834 10.9024 19.3166 11.2929 19.7071C11.6834 20.0976 12.3166 20.0976 12.7071 19.7071L19.7071 12.7071C20.0976 12.3166 20.0976 11.6834 19.7071 11.2929L12.7071 4.29289Z" fill="white"/></svg>', '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M12.7071 4.29289C12.3166 3.90237 11.6834 3.90237 11.2929 4.29289C10.9024 4.68342 10.9024 5.31658 11.2929 5.70711L16.5858 11H5C4.44772 11 4 11.4477 4 12C4 12.5523 4.44772 13 5 13H16.5858L11.2929 18.2929C10.9024 18.6834 10.9024 19.3166 11.2929 19.7071C11.6834 20.0976 12.3166 20.0976 12.7071 19.7071L19.7071 12.7071C20.0976 12.3166 20.0976 11.6834 19.7071 11.2929L12.7071 4.29289Z" fill="white"/></svg>'],
      responsive: {
        320: {
          items: 2,
          margin: 0
        },
        375: {
          items: 3
        },
        640: {
          items: 5
        },
        1280: {
          items: 5
        }
      }
    });
  }

  $('.js-title-mobile').on('click', function () {
    $(this).toggleClass('is-active-title');
    $(this).siblings('ul').slideToggle();
  });

  function menuMore() {
    $('.main-menu__nav ul').oneLineMenu({
      minWidth: 1024
    });
    $('.dropdown-wrap').on('click', function () {
      $(this).find('ul').stop().fadeToggle();
    });
    $(document).on('click', function (e) {
      if ($(e.target).closest('.main-menu__nav').length) {
        return;
      }

      $('.dropdown-wrap ul').fadeOut();
    });
  }

  menuMore();
  $('.js-btn-burger').on('click', function () {
    $('.js-menu-show').toggleClass('is-active-menu');
    $('.js-mobile-menu').toggleClass('is-active-mobile-menu');
  });
  $('.js-btn-close').on('click', function () {
    $('.js-menu-show').removeClass('is-active-menu');
    $('.js-mobile-menu').removeClass('is-active-mobile-menu');
  });
  var PAGE = $('html, body');
  var pageScroller = $('.page-scroller');
  var inMemoryClass = 'page-scroller--memorized';
  var isVisibleClass = 'page-scroller--visible';
  var enabledOffset = 60;
  var pageYOffset = 0;
  var inMemory = false;

  function resetPageScroller() {
    setTimeout(function () {
      if (window.pageYOffset > enabledOffset) {
        pageScroller.addClass(isVisibleClass);
      } else if (!pageScroller.hasClass(inMemoryClass)) {
        pageScroller.removeClass(isVisibleClass);
      }
    }, 150);

    if (!inMemory) {
      pageYOffset = 0;
      pageScroller.removeClass(inMemoryClass);
    }

    inMemory = false;
  }

  if (pageScroller.length > 0) {
    window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
      passive: true
    } : false);
    pageScroller.on('click', function (event) {
      event.preventDefault();
      window.removeEventListener('scroll', resetPageScroller);

      if (window.pageYOffset > 0 && pageYOffset === 0) {
        inMemory = true;
        pageYOffset = window.pageYOffset;
        pageScroller.addClass(inMemoryClass);
        PAGE.stop().animate({
          scrollTop: 0
        }, 500, 'swing', function () {
          window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
            passive: true
          } : false);
        });
      } else {
        pageScroller.removeClass(inMemoryClass);
        PAGE.stop().animate({
          scrollTop: pageYOffset
        }, 500, 'swing', function () {
          pageYOffset = 0;
          window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
            passive: true
          } : false);
        });
      }
    });
  }

  $('.js-phone-mask').mask('+7(000)0000000');
  $('.js-btn-back').on('click', function () {
    $('[data-remodal-id=call]').remodal().close();
  }); //btn-back Video modal

  $('.js-btn-back').on('click', function () {
    $('[data-remodal-id=video]').remodal().close();
  }); //btn-back Video modal END

  function ProductSeriesImgPath() {
    var getProductSeriesImgPath = $('.js-product-series-img-path').find('img').attr('src'),
        styleInlineBgImg = 'background-image: url(' + getProductSeriesImgPath + ');';
    $('.js-product-series-img-path').attr('style', styleInlineBgImg);
  }

  ProductSeriesImgPath();

  if (window.matchMedia("(max-width: 1023px)").matches) {
    $('.js-product-series-head').insertBefore('.product-series__section');
  }

  $('.products-modification__tabs-switches').on('click', '.js-tabs-swither:not(.is-active-tabs-swither)', function () {
    $(this).addClass('is-active-tabs-swither').siblings().removeClass('is-active-tabs-swither');
    $('.products-modification__tabs-container').find('.js-tabs-content').removeClass('is-active-tabs-content').hide().eq($(this).index()).fadeIn();
  });
  $('.js-btn-archive').on('click', function () {
    $(this).toggleClass('is-active-btn-archive');
    $('.js-archive-content').slideToggle();
  });

  if (window.matchMedia("(max-width: 479px)").matches) {
    $('.js-btn-archive').find('span').text('архивные модификации');
  }

  $('.js-btn-search').on('click', function () {
    $('.search').fadeToggle();
  });

  function selectSityType2() {
    var selectedSity = $('.js-selected-sity-type2'),
        listSity = $('.js-list-sity-type2');
    selectedSity.on('click', function () {
      listSity.stop().slideToggle();
    });
    $(document).on('click', function (e) {
      if ($(e.target).closest('.select-sity-type2').length) {
        return;
      }

      listSity.slideUp();
    });
  }

  selectSityType2();

  function selectSity() {
    var selectedSity = $('.js-selected-sity'),
        listSity = $('.js-list-sity');

    if (window.matchMedia("(min-width: 1025px)").matches) {
      selectedSity.on('click', function () {
        listSity.stop().fadeToggle();
      });
      $(document).on('click', function (e) {
        if ($(e.target).closest('.select-sity').length) {
          return;
        }

        listSity.fadeOut();
      });
    } else {
      selectedSity.on('click', function () {
        listSity.stop().slideToggle();
      });
      $(document).on('click', function (e) {
        if ($(e.target).closest('.select-sity').length) {
          return;
        }

        listSity.slideUp();
      });
    }
  }

  selectSity();

  $.fn.digitsFilter = function () {
    $(this).on('keydown', function (e) {
      // Allow: backspace, delete, tab, escape, enter and //.
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 || //, 110, 190
      // Allow: Ctrl+A, Command+A
      e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true) || // Allow: home, end, left, right, down, up
      e.keyCode >= 35 && e.keyCode <= 40) {
        return;
      } // Ensure that it is a number and stop the keypress


      if ((e.shiftKey || e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
      }
    });
  };

  $('.ui-filter__range').each(function () {
    var $range = $(this);
    var $inputs = $range.closest('.ui-filter__range-controlls').find('.from, .to');
    var $inputMin = $inputs.eq(0);
    var $inputMax = $inputs.eq(1);

    function setToInputsRangeData($_this, upd) {
      var from = $_this.data("from"),
          to = $_this.data("to"),
          postfix = $_this.data('postfix');

      if (upd) {
        $inputMin.val(postfix ? from + postfix : from);
        $inputMax.val(postfix ? to + postfix : to);
      }
    }

    $inputs.digitsFilter();
    $range.ionRangeSlider({
      onStart: function onStart() {
        setToInputsRangeData($range, true);
      }
    }); //--------------------------------------

    var updateInputs = true;
    $range.on("change", function () {
      setToInputsRangeData($range, updateInputs);
      updateInputs = true;
    });
    $inputs.on('change', function () {
      //accounting.formatNumber(4999.99, 0, " ");
      updateInputs = false;
      var inputMinVal = Number($inputMin.val().replace(/\D+/g, ''));
      var inputMaxVal = Number($inputMax.val().replace(/\D+/g, ''));
      var postfix = $range.data('postfix') || 0;

      if ($(this)[0] == $inputMin[0]) {
        if (inputMinVal > inputMaxVal) {
          $inputMin.val(inputMaxVal + postfix);
        }

        if (inputMinVal < $range.data('min')) {
          $inputMin.val($range.data('min') + postfix);
        }
      } else {
        if (inputMaxVal < inputMinVal) {
          $inputMax.val(inputMinVal + postfix);
        }

        if (inputMaxVal > $range.data('max')) {
          $inputMax.val($range.data('max') + postfix);
        }
      }

      $range.data('ionRangeSlider').update({
        from: $inputMin.val().replace(/\D+/g, ''),
        to: $inputMax.val().replace(/\D+/g, '')
      });
    });
  });
  $('.js-ui-filter-title').on('click', function () {
    $('.js-ui-filter-form').slideToggle();
  });

  if (window.matchMedia("(min-width: 960px)").matches) {
    $(".ui-filter").stick_in_parent({
      offset_top: 111
    });
  }

  $('.js-btn-show-more').on('click', function (event) {
    event.preventDefault();
    $('.js-show-more').addClass('show-more');
    $(this).hide();
  }); // filter docs

  $('.js-item-doc').on('click', function () {
    $('.js-docs-content').hide();
    var $thisItem = $(this),
        $thisData = $thisItem.attr('data-filterDoc');

    if ($thisData === "certificates") {
      $thisItem.addClass('is-active-doc');
      $thisItem.siblings().removeClass('is-active-doc');
      $('[data-docContent=certificates]').show();
    }

    if ($thisData === "passports") {
      $thisItem.addClass('is-active-doc');
      $thisItem.siblings().removeClass('is-active-doc');
      $('[data-docContent=passports]').show();
    }

    if ($thisData === "ies") {
      $thisItem.addClass('is-active-doc');
      $thisItem.siblings().removeClass('is-active-doc');
      $('[data-docContent=ies]').show();
    }
  }); // function ieVersion() {
  //     var ua = window.navigator.userAgent;
  //     if (ua.indexOf("Trident/7.0") > -1) {
  //         $('html').hide();
  //     }
  //     console.log(ua, '+')
  // } 
  // ieVersion();
  // var uiFilter = $('.ui-filter');
  // $(window).scroll(function () {
  //   if ($(this).scrollTop() > 350) {
  //     uiFilter.addClass('fixed-filter-active');
  //   } else if ($(this).scrollTop() <= 350) {
  //     uiFilter.removeClass('fixed-filter-active');
  //   }
  // });
  // if (window.matchMedia("(min-width: 1360px)").matches) {
  //   $(function() {
  //     var $window = $(window);
  //     var $sidebar = $(".ui-filter");
  //     var $sidebarTop = $sidebar.position().top;
  //     var $sidebarHeight = $sidebar.height();
  //     var $footer = $('.questions');
  //     var $footerTop = $footer.position().top;
  //     $window.scroll(function(event) {
  //       $sidebar.addClass("fixed-filter-active");
  //       var $scrollTop = $window.scrollTop() + 111;
  //       var $topPosition = Math.max(0, $sidebarTop - $scrollTop);
  //       if ($scrollTop + $sidebarHeight > $footerTop) {
  //         var $topPosition = Math.min($topPosition, $footerTop - $scrollTop - $sidebarHeight);
  //       }
  //       $sidebar.css("top", $topPosition + 111);
  //       if ($scrollTop <= 41) {
  //         $sidebar.removeClass("fixed-filter-active");
  //       }
  //     });
  //   });
  // }
  // Video Modal

  var videoModal = $('[data-remodal-id=video]').remodal();
  $('.js-preview').on('click', function (event) {
    event.preventDefault();
    var $this = $(this),
        videoSrc = $this.attr('href'),
        hiddenTitle = $this.find('.hidden-title-video').clone(),
        hiddenText = $this.find('.hidden-text-video').clone(),
        inRemodalVideo = $this.closest('.app').find('.popup-video');
    inRemodalVideo.find('iframe').attr('src', videoSrc);
    inRemodalVideo.find('.js-inside-title').append(hiddenTitle);
    inRemodalVideo.find('.js-inside-text').append(hiddenText);
    videoModal.open();
    console.log(hiddenTitle);
  });
  $(document).on('closed', '[data-remodal-id=video]', function (e) {
    $('[data-remodal-id="video"] iframe').attr('src', '');
    $('[data-remodal-id="video"] .js-inside-title').find('.hidden-title-video').remove();
    $('[data-remodal-id="video"] .js-inside-text').find('.hidden-text-video').remove();
  }); // Video Modal END

  var productSlider = $('.card-product-slider__top-slider'),
      productItems = productSlider.find('.card-product-slider__top-slider-item'),
      productThumbs = $('.card-product-slider__bottom-slider');
  $.each(productItems, function (i, el) {
    var classActive = i == 0 ? 'is-active' : '';
    var item = $('<div class="card-product-slider__bottom-slider-item ' + classActive + '" data-index="' + i + '"><img src="' + $(el).data('thumb') + '" alt=""></div>');
    item.appendTo(productThumbs);
  });
  productSlider.addClass('owl-carousel').owlCarousel({
    mouseDrag: false,
    items: 1,
    animateOut: 'fadeOut',
    nav: true,
    dots: false,
    navText: ['<svg width="11" height="18" viewBox="0 0 11 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 17L9 9L0.999999 1" stroke="white" stroke-width="2"/></svg>', '<svg width="11" height="18" viewBox="0 0 11 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 17L9 9L0.999999 1" stroke="white" stroke-width="2"/></svg>'],
    onChanged: function onChanged(event) {
      var $thisIndex = event.item.index;
      $('.card-product-slider__bottom-slider-item').removeClass('is-active');
      $('.card-product-slider__bottom-slider-item[data-index="' + $thisIndex + '"]').addClass('is-active');
    },
    onInitialize: function onInitialize(event) {
      productThumbs.addClass('owl-carousel').owlCarousel({
        items: 4,
        margin: 4,
        nav: false,
        navText: ['<svg width="11" height="18" viewBox="0 0 11 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 17L9 9L0.999999 1" stroke="white" stroke-width="2"/></svg>', '<svg width="11" height="18" viewBox="0 0 11 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 17L9 9L0.999999 1" stroke="white" stroke-width="2"/></svg>'],
        mouseDrag: false
      });
    }
  });
  $('body').on('click', '.card-product-slider__bottom-slider-item', function () {
    productSlider.trigger('to.owl.carousel', [$(this).data('index'), 300]);
    $('.card-product-slider__bottom-slider-item').removeClass('is-active');
    $('.card-product-slider__bottom-slider-item[data-index="' + $(this).data('index') + '"]').addClass('is-active');
  });

  if (window.matchMedia("(max-width: 959px)").matches) {
    $('.js-card-product-head').insertBefore('.card-product__section');
    $('.js-card-product-btns').insertAfter('.card-product-slider');
  }

  $('input[type=checkbox], select').styler();
});